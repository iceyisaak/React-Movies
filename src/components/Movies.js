import React,{ useState, useEffect } from 'react';
import { Table, Button } from 'react-bootstrap';
import { API_URL, API_KEY, SEARCH_FOR } from '../utils/config';
import { MovieList } from './MovieList';

export const Movies = () => {

    const [movies, setMovies] = useState([]);
    const [totalResults, setTotalResults] = useState(0);   
    const [maxPage, setMaxPage] = useState(0);
    const [pageNo, setPageNo] = useState(1);
    const [search, setSearch] = useState(SEARCH_FOR);
 
    useEffect(() => {       
        
        if (!localStorage.getItem('Page_' + {pageNo})  ) {   
            setSearch(SEARCH_FOR);      
            fetchMoviesbySearch(search);
        } else {          
            let data = localStorage.getItem('Page_' + {pageNo})
            setMovies(JSON.parse(data));
        }        
    }, [pageNo]);
  
    const fetchMoviesbySearch = async (Search) => {
        const response = await fetch(`${API_URL}?apikey=${API_KEY}&s=${search}&page=${pageNo}`)
        const res = await response.json()         
        if (res.Response === "True") {
            localStorage.setItem('Page_' + pageNo,JSON.stringify(res.Search)); 
            setMovies(res.Search);
            if (totalResults === 0) {
                setTotalResults(res.totalResults)
                setMaxPage(Math.floor(res.totalResults  / 10) )
              };                           
        } else {           
            console.log("fetchMoviesbySearch Error : " + res.Error);              
        }         
    };   

      
    return(
      <>
        <Table className="mt-4" striped bordered hover size="sm">
            <thead>
                <tr>                   
                    <th>Poster</th>
                    <th>Title</th>
                    <th>Plot</th>
                    <th>Year</th>
                    <th>Edit</th>
                </tr>
            </thead>
            <tbody>               
                { 
                    movies.map((movie,index)=> {
                        const { imdbID } = movie
                        return(
                                <tr key = {imdbID} >                    
                                    <MovieList movieID ={ imdbID }  />                      
                                </tr>
                            )
                        }
                    )                                                   
                }                
            </tbody> 
        </Table>
             
        <Button variant="outline-success" size="sm"  disabled={pageNo ===1} onClick={() => setPageNo(1) }>
                First Page
        </Button>
        <Button variant="success" size="sm" disabled={pageNo ===1} onClick = {() => pageNo > 1 ? setPageNo(pageNo - 1): setPageNo(1) } >
                Previous Page
        </Button>
        <Button variant="success" size="sm" disabled={pageNo === maxPage} onClick={() => pageNo < maxPage ? setPageNo(pageNo + 1) : setPageNo(maxPage) }>
                Next Page
        </Button>
        <Button variant="outline-success" size="sm" disabled={pageNo === maxPage} onClick={() => setPageNo(maxPage) }>
                Last Page
        </Button>
        <span variant="outline-primary" size="sm" disabled={true} >
            Page {pageNo} of {maxPage} | 
        </span>
        <span variant="outline-primary" size="sm" disabled={true} >
            Total Results : {totalResults} 
        </span>  
        <Button variant="danger" size="sm" onClick={() => localStorage.clear() }>
              Clear LocalStorage
        </Button>     
           
      </>  
    );
};