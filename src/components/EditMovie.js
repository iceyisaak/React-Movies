import React,{ useState } from 'react';
import { Modal, Button, Row, Col, Form } from 'react-bootstrap';
import { NO_POSTER } from '../utils/config';

export const EditMovie = ( props ) => {

    const { imdbID, Title, Poster, Plot, Year } = props.movie;
    const { newPlot, setNewPlot } = useState(props.movie.Plot);

    const saveMovie = (e) => {
          e.preventDefault();
          props.movie.Plot = e.target.value
          localStorage.setItem('ID_'  + imdbID,JSON.stringify(props.movie));   
    }

    return (
      <Modal
      {...props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Edit Movie
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="container">
              <Row>
                  <Col sm={12} >
                      <Form onSubmit={saveMovie}>
                      <Form.Group controlid="poster">
                            <img 
                            width="200"                            
                            src={Poster === 'N/A' ? NO_POSTER : Poster}
                            alt={'No Poster'} 
                            />
                        </Form.Group>
                        <Form.Group controlid="title">
                            <Form.Label>Title (Year : {Year})</Form.Label>
                            <Form.Control
                              type="text"
                              disabled
                              name="Title"
                              defaultValue={Title}
                            />
                        </Form.Group>                       
                        <Form.Group controlid="plot">
                            <Form.Label>Plot</Form.Label>
                            <Form.Control
                              type="text"
                              as='textarea'                            
                              name="Plot"
                              requiredplaceholder="Plot"
                              defaultValue={Plot}
                              value={newPlot}
                              onChange={saveMovie}
                            />
                        </Form.Group> 
                      </Form>
                  </Col>
              </Row>
          </div>
          
        </Modal.Body>
        <Modal.Footer>        
            <Button onClick={props.onHide}>Save & Close</Button> 
        </Modal.Footer>
      </Modal>
    );
  }