import React ,{ useState, useEffect } from "react";
import { Button, ButtonToolbar } from 'react-bootstrap';

import { API_URL, API_KEY, NO_POSTER } from '../utils/config';
import { EditMovie } from './EditMovie';

export const MovieList = ( {movieID} ) => {
 
  const [movieDetail, setmovieDetail] = useState([]);  
  const [modalShow, setModalShow] = useState(false);
  const { imdbID, Title, Poster, Plot, Year } = movieDetail;
 

  useEffect(() => {
      
      if  (!localStorage.getItem('ID_'  + movieID)) {
        fetchMoviesbyID();      
      } else {
        let data = localStorage.getItem('ID_'  + movieID)
        setmovieDetail(JSON.parse(data));
      }

    },[]); 

    const fetchMoviesbyID = async () => {
      const response = await fetch(`${API_URL}?apikey=${API_KEY}&i=${movieID}&plot=short`)
      const res = await response.json()
      if (res.Response === "True") {
        if ('ID_'  + movieID !== 'ID_undefined') {
          setmovieDetail(res);            
          localStorage.setItem('ID_'  + movieID,JSON.stringify(res)); 
         }                        
      } else {           
        console.log("fetchMoviesbyID Error : " + res.Error);                 
      }   
  };

  return (        
      <>     
          <td>            
              <img 
                width="25" 
                src={Poster === 'N/A' ? NO_POSTER : Poster}
                alt={'No Poster'} 
              />
          </td>
          <td>{Title}</td>
          <td>{Plot}</td>  
          <td>{Year}</td>
          <td>
              <ButtonToolbar>
                  <Button variant='info' onClick={() => setModalShow(true)}>Edit</Button>
                  <EditMovie show={modalShow} onHide={() => setModalShow(false)} movie = {movieDetail}  />
              </ButtonToolbar>
          </td>
      </>
      
  );
};

