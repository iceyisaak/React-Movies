import React, { Component } from 'react';

import './App.css';
import { Header } from './components/Header';
import { Movies } from './components/Movies';

//import { Button, ButtonToolbar } from 'react-bootstrap';

class App extends Component {
    
    render () {
        return (
            <div >
                <Header text="React Movies App" />
                <Movies />     
            </div>
        )
    }
}

export default App;
